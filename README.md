# CRUD API using Cosmos DB and Azure Blob Storage

Project is based on C# .NET Core 3.1. Integration testing with [xUnit](https://xunit.net/).

This project is a CRUD API using Azure Cosmos Db for storing datas. For this project, I decided to use directly the CosmosClient in the repository. 
A target archicture could be a generic CosmosClient where we specify generic CRUD operations with templating. So we could use generic methods which handles errors accros the API. Every repositories will implement this custom CosmosDb interface.

We store files to an Azure Blob Storage. We can upload and update our files.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

For testing purposes, two packages are necessary: [FluentAssertions](https://www.nuget.org/packages/FluentAssertions/) and [moq](https://www.nuget.org/packages/moq/).

For CosmosDb, [Microsoft.Azure.Cosmos](https://www.nuget.org/packages/Microsoft.Azure.Cosmos) and an [Azure CosmosDb account](https://docs.microsoft.com/fr-fr/azure/cosmos-db/how-to-manage-database-account).

For online storage, [Azure Blob Storage](https://www.nuget.org/packages/Azure.Storage.Blobs).

### Installing

Clone using

```bash
git clone https://gitlab.com/cdk-cdk/cosmosdb-crud-api.git
```

Open a terminal and type

```powershell
cd .\Tacx.Activities.Api\
dotnet restore .\Tacx.Activities.Api.csproj
dotnet run
```

Or run the solution with your favorite IDE, Visual Studio 2019 was used for this project.

#### Connect the API to your Azure Cosmos Db account

Add your PrimaryKey in Tacx.Activities.Api\Tacx.Activities.Api\appsettings.json
Replace the following code with your values: 
```json
  "CosmosDbSettings": {
    "URL": "https://cedrickcosmosdb.documents.azure.com:443/",
    "DatabaseName": "CompanyDatabase",
    "ContainerName": "Activities"
  }
```
to
```json
  "CosmosDbSettings": {
    "URL": "Your Azure URI",
    "DatabaseName": "Your Database Name",
    "ContainerName": "Your Container Name",
    "PrimaryKey": "Your Key"
  }
```

You could decide to not put your primary key in the app.settings and add it as a secret.
  
```powershell
cd .\Tacx.Activities.Api\
dotnet user-secrets init
dotnet user-secrets set CosmosDB:PrimaryKey H******LA==
```

#### Verify deployment

Verify the deployment by navigating to the swagger address in your preferred browser.

```bash
http://localhost:5000/swagger/index.html
or 
https://localhost:5001/swagger/index.html
```

## Endpoints
Please also review the swagger on http://localhost:5000/swagger/index.html.

### Add an activity
```bash
(POST) http://localhost:5000/api/activities
```
Use provided collection to test this endpoint. Examples files are available in the folder located at Postman/Files.
![postman create activity screenshot](Postman/createActivity.PNG)

### Get an activity

```bash
(GET) http://localhost:5000/api/activities/{id}

examples:
(GET) http://localhost:5000/api/activities/202103191436380001
```

### Update an activity
It returns the updated activity.
Use provided collection in the Postman folder to test this endpoint. Examples json files are available in the folder located at Postman/Files.

```bash
(PUT) http://localhost:5000/api/activities
```
![postman create activity screenshot](Postman/updateActivity.PNG)

### Delete an activity

```bash
(DELETE) http://localhost:5000/api/activities/{id}

examples:
(DELETE) http://localhost:5000/api/activities/202103191436380001
```
## Testing

At the root folder, open a terminal and type

```powershell
cd .\Tacx.Activities.Api.Tests\
dotnet restore .\Tacx.Activities.Api.Tests.csproj
dotnet test
```

The result shall be: 

```powershell
Passed!  - Failed:     0, Passed:    10, Skipped:     0, Total:    10, Duration: 98 ms - Tacx.Activities.Api.Tests.dll (netcoreapp3.1)
```