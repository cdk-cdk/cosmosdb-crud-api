﻿using Microsoft.Azure.Cosmos;
using System;
using System.Threading.Tasks;
using Tacx.Activities.Api.Models;

namespace Tacx.Activities.Api.Repository
{
    /// <summary>
    /// Activities repository
    /// </summary>
    public class ActivitiesRepository : IActivitiesRepository
    {
        #region Instance variables

        private readonly Container _container;

        #endregion

        #region Constructor

        /// <summary>
        /// Repository constructor
        /// </summary>
        /// <param name="client">CosmosDb initialized client</param>
        /// <param name="databaseName">Name of the database we will use</param>
        /// <param name="containerName">Name of the container used</param>
        public ActivitiesRepository(CosmosClient client, string databaseName, string containerName)
        {
            this._container = client.GetContainer(databaseName, containerName);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Create an activity in the database
        /// </summary>
        /// <param name="activity">New activity</param>
        public async Task<bool> AddActivityAsync(Activity activity)
        {
            using (ResponseMessage responseMessage = await this._container.ReadItemStreamAsync(activity.Id, new PartitionKey(activity.Id)))
            {
                if (responseMessage.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    await this._container.CreateItemAsync<Activity>(activity, new PartitionKey(activity.Id));
                    return (true);
                }

                if (responseMessage.IsSuccessStatusCode)
                {
                    return (false);
                }
            }

            return (false);
        }

        /// <summary>
        /// Deletes an activity
        /// </summary>
        /// <param name="activityId">Id of the activity we want to delete</param>
        /// <returns>Task</returns>
        public async Task DeleteActivityByIdAsync(string activityId)
        {
            await this._container.DeleteItemAsync<Activity>(activityId, new PartitionKey(activityId));
        }

        /// <summary>
        /// Get a specific activity from the database
        /// </summary>
        /// <param name="activityId">ActivityId</param>
        /// <returns>Activity or null</returns>
        public async Task<Activity> GetActivityByIdAsync(string id)
        {
            try
            {
                ItemResponse<Activity> response = await this._container.ReadItemAsync<Activity>(id, new PartitionKey(id));
                return response.Resource;
            }
            catch (CosmosException ex) when (ex.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
        }

        /// <summary>
        /// Update an activity
        /// </summary>
        /// <param name="activity">Activity to update</param>
        public async Task UpdateActivityAsync(Activity activity)
        {
            try
            {
                await this._container.UpsertItemAsync(activity, new PartitionKey(activity.Id));
            }
            catch (CosmosException ex)
            {
                Console.WriteLine(ex);
            }
        }

        #endregion
    }
}
