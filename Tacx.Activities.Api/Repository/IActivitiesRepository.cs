﻿using System.Threading.Tasks;
using Tacx.Activities.Api.Models;

namespace Tacx.Activities.Api.Repository
{
    /// <summary>
    /// Activities repository interface
    /// </summary>
    public interface IActivitiesRepository
    {
        /// <summary>
        /// Create an activity in the database
        /// </summary>
        /// <param name="activity">New activity</param>
        Task<bool> AddActivityAsync(Activity activity);

        /// <summary>
        /// Get a specific activity from the database
        /// </summary>
        /// <param name="activityId">ActivityId</param>
        /// <returns>Activity</returns>
        Task<Activity> GetActivityByIdAsync(string activityId);

        /// <summary>
        /// Update an activity
        /// </summary>
        /// <param name="activity">Activity to update</param>
        Task UpdateActivityAsync(Activity item);

        /// <summary>
        /// Delete an activity from it's id
        /// </summary>
        /// <param name="activityId">Id of the activity we want to delete</param>
        Task DeleteActivityByIdAsync(string activityId);
    }
}
