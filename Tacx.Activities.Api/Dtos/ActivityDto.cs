﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tacx.Activities.Api.Dtos
{
    public class ActivityDto
    {
        #region Properties

        [Required]
        /// <summary>
        /// Get or set the activity id
        /// This property is mandatory
        /// </summary>
        public string ActivityId { get; set; }

        /// <summary>
        /// Get or set the average speed in meter/second
        /// </summary>
        public double AvgSpeed { get; set; }

        /// <summary>
        /// Get or set the date when the activity occured
        /// </summary>
        public DateTimeOffset CreatedDate { get; set; }

        /// <summary>
        /// Get or set the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Get or set the distance in meters
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// Get or set the duration in seconds
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Get or set the name
        /// </summary>
        public string Name { get; set; }

        #endregion
    }
}
