﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;

namespace Tacx.Activities.Api.Services
{
    /// <summary>
    /// Service interacting with Azure File Shares
    /// </summary>
    public class FilesStorageService : IFilesStorageService
    {
        #region Instance variables

        private readonly string _connectionString;
        private readonly string _containerName;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString">A connection string to Azure Storage account.</param>
        /// <param name="connectionString">Name of the container we will use</param>
        public FilesStorageService(string connectionString, string containerName)
        {
            this._connectionString = connectionString;
            this._containerName = containerName;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Delete a file on the azure blob storage
        /// </summary>
        /// <param name="blobReference">Name of the file we want to download without json extension</param>
        /// <returns></returns>
        public async Task<bool> DeleteFile(string blobReference)
        {
            blobReference += ".json";
            BlobContainerClient blobContainerClient = new BlobContainerClient(this._connectionString, this._containerName);
            BlobClient blob = blobContainerClient.GetBlobClient(blobReference);
            return await blob.DeleteIfExistsAsync();
        }

        /// <summary>
        /// Download the file from the blob storage
        /// </summary>
        /// <param name="blobReference">Name of the file we want to download without json extension</param>
        /// <returns>Path where to find the file.</returns>
        public async Task<string> DownloadFile(string blobReference)
        {
            blobReference += ".json";
            BlobContainerClient blobContainerClient = new BlobContainerClient(this._connectionString, this._containerName);
            BlobClient blob = blobContainerClient.GetBlobClient(blobReference);

            var path = Path.GetTempPath() + blobReference;
            await blob.DownloadToAsync(path);

            return (path);
        }

        /// <summary>
        /// Upload a file
        /// Create the file or update it if it already exists
        /// </summary>
        /// <param name="fileName">File name and also the name of the share to create and upload.</param>
        /// <param name="file">Incoming file to upload</param>
        /// <returns>Task</returns>
        public async Task<bool> UploadAsync(string fileName, IFormFile file)
        {
            BlobContainerClient blobContainerClient = new BlobContainerClient(this._connectionString, this._containerName);
            await blobContainerClient.CreateIfNotExistsAsync();

            // Checking if the file exists 
            // if the file exists it will be replaced
            // if it doesn't exist it will create a temp space until its uploaded

            BlobClient blobClient = blobContainerClient.GetBlobClient(fileName.ToLowerInvariant() + Path.GetExtension(file.FileName));

            var httpHeaders = new BlobHttpHeaders()
            {
                ContentType = file.ContentType
            };

            // Upload File

            try
            {
                var res = await blobClient.UploadAsync(file.OpenReadStream(), httpHeaders);
                if (res != null)
                    return (true);
            }
            catch (RequestFailedException ex)
                when (ex.ErrorCode == BlobErrorCode.BlobAlreadyExists)
            {
                // Ignore any errors if the blob already exists

                return (false);
            }

            return (false);
        }

        #endregion

    }
}
