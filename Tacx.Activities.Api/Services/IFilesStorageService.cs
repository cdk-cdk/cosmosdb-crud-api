﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Tacx.Activities.Api.Services
{
    /// <summary>
    /// Interface specifying methods to interact with our online storage
    /// </summary>
    public interface IFilesStorageService
    {
        /// <summary>
        /// Delete the file on the blob storage
        /// </summary>
        /// <param name="blobReference">Name of the file we want to delete without json extension</param>
        /// <returns>TRUE success, false otherwise</returns>
        Task<bool> DeleteFile(string blobReference);

        /// <summary>
        /// Download the file from the blob storage
        /// </summary>
        /// <param name="blobReference">Name of the file we want to download without json extension</param>
        /// <returns>Path where to find the file.</returns>
        Task<string> DownloadFile(string blobReference);

        /// <summary>
        /// Upload a file
        /// Create or update the file
        /// </summary>
        /// <param name="fileName">File name and also the name of the share to create and upload</param>
        /// <param name="file">Incoming file to upload</param>
        /// <returns>Task</returns>
        Task<bool> UploadAsync(string fileName, IFormFile file);
    }
}
