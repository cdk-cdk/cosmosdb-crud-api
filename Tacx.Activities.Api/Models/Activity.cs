﻿using System;
using Tacx.Activities.Api.Dtos;

namespace Tacx.Activities.Api.Models
{
    public class Activity
    {
        #region Properties

        /// <summary>
        /// Get or set the average speed in meter/second
        /// </summary>
        public double AvgSpeed { get; set; }

        /// <summary>
        /// Get or set the date when the activity occured
        /// </summary>
        public DateTimeOffset CreatedDate { get; set; }

        /// <summary>
        /// Get or set the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Get or set the distance in meters
        /// </summary>
        public double Distance { get; set; }

        /// <summary>
        /// Get or set the duration in seconds
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Get or set the activity id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Get or set the name
        /// </summary>
        public string Name { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor to transform an ActivityDto into an Activity
        /// </summary>
        /// <param name="dto">ActivityDto</param>
        public Activity(ActivityDto dto)
        {
            this.AvgSpeed = dto.AvgSpeed;
            this.CreatedDate = dto.CreatedDate;
            this.Description = dto.Description;
            this.Distance = dto.Distance;
            this.Duration = dto.Duration;
            this.Id = dto.ActivityId;
            this.Name = dto.Name;
        }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public Activity()
        {

        }

        #endregion
    }
}
