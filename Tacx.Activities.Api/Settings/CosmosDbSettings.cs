﻿namespace Tacx.Activities.Api.Settings
{
    public class CosmosDbSettings
    {
        public string Url { get; set; }
        public string PrimaryKey { get; set; }
        public string DatabaseName { get; set; }
        public string ContainerName { get; set; }
        public string PartitionKey { get; set; }
    }
}
