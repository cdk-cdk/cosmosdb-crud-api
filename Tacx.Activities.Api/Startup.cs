using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Tacx.Activities.Api.Repository;
using Tacx.Activities.Api.Services;
using Tacx.Activities.Api.Settings;

namespace Tacx.Activities.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                options.SuppressAsyncSuffixInActionNames = false;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Activities", Version = "v1" });
            });

            // Azure Cosmos Db

            var cosmosDbSettings = Configuration.GetSection(nameof(CosmosDbSettings)).Get<CosmosDbSettings>();
            services.AddSingleton<IActivitiesRepository>(InitializeCosmosClientInstanceAsync(cosmosDbSettings).GetAwaiter().GetResult());

            // Azure Blob Storage 

            services.AddScoped<IFilesStorageService>(s => new FilesStorageService(Configuration["AzureStorageSettings:ConnectionString"], Configuration["AzureStorageSettings:ContainerName"]));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Activities v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        /// <summary>
        /// Create a CosmosDb client and set it in a the repository
        /// </summary>
        /// <param name="settings">CosmosDb settings</param>
        /// <returns>The repository set with a cosmosDb client</returns>
        private static async Task<ActivitiesRepository> InitializeCosmosClientInstanceAsync(CosmosDbSettings settings)
        {
            CosmosClient client = new CosmosClient(settings.Url, settings.PrimaryKey, new CosmosClientOptions()
            {
                SerializerOptions = new CosmosSerializationOptions()
                {
                    PropertyNamingPolicy = CosmosPropertyNamingPolicy.CamelCase
                }
            }
            );
            Database database = await client.CreateDatabaseIfNotExistsAsync(settings.DatabaseName);
            await database.CreateContainerIfNotExistsAsync(settings.ContainerName, settings.PartitionKey);
            return new ActivitiesRepository(client, settings.DatabaseName, settings.ContainerName);
        }
    }
}
