﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tacx.Activities.Api.Dtos;
using Tacx.Activities.Api.Models;
using Tacx.Activities.Api.Repository;
using Tacx.Activities.Api.Services;

namespace Tacx.Activities.Api.Controllers
{
    /// <summary>
    /// Activities controller
    /// </summary>
    [Route("api/activities")]
    [ApiController]
    public class ActivitiesController : ControllerBase
    {
        private readonly IActivitiesRepository _activitiesRepository;
        private readonly IFilesStorageService _filesStorageService;

        #region Constructor

        /// <summary>
        /// Controller constructor
        /// </summary>
        /// <param name="activitiesRepository">Repository allowing us to interact with the database</param>
        /// <param name="filesStorageService">Service allowing us to interact with the online storage</param>
        public ActivitiesController(IActivitiesRepository activitiesRepository, IFilesStorageService filesStorageService)
        {
            this._activitiesRepository = activitiesRepository;
            this._filesStorageService = filesStorageService;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add an activity in the database and upload the file in our online storage
        /// </summary>
        /// <param name="file">File to uload</param>
        /// <returns>The new activity with 201 http code or an error</returns>
        [HttpPost]
        public async Task<ActionResult<ActivityDto>> CreateActivityAsync([FromForm] IFormFile file)
        {
            if (file == null || file.Length < 1)
            {
                return BadRequest("File is empty.");
            }

            // Extract object from incoming JSON file 

            ActivityDto activity = Helper.DeserializeFromStream<ActivityDto>(file.OpenReadStream());
            Activity newActivity = new Activity(activity);

            // Upload data to our database

            if (!(await this._activitiesRepository.AddActivityAsync(newActivity)))
            {
                return Conflict();
            }

            // Upload the file to our online storage

            if (!(await this._filesStorageService.UploadAsync(newActivity.Id, file)))
            {
                await this._activitiesRepository.DeleteActivityByIdAsync(newActivity.Id);
                return StatusCode(500);
            }

            // Calls GetActivityById endpoint to ensures that we uploaded the file

            return CreatedAtAction(nameof(GetActivityByIdAsync), new { id = newActivity.Id }, newActivity.AsDto());
        }

        /// <summary>
        /// Delete activity in our database
        /// </summary>
        /// <param name="id">ID of the activity we want to delete</param>
        /// <returns>Task</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteActivityByIdAsync(string id)
        {
            if (String.IsNullOrEmpty(id))
                return BadRequest();

            Activity activity = await this._activitiesRepository.GetActivityByIdAsync(id);

            if (activity == null)
                return NotFound();

            await this._activitiesRepository.DeleteActivityByIdAsync(id);

            await this._filesStorageService.DeleteFile(id);

            return NoContent();
        }

        /// <summary>
        /// Get a specific activity from the database
        /// </summary>
        /// <param name="id">ActivityId</param>
        /// <returns>The activity or an error.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<string>> GetActivityByIdAsync(string id)
        {
            if (String.IsNullOrEmpty(id))
                return BadRequest();

            Activity activity = await this._activitiesRepository.GetActivityByIdAsync(id);

            if (activity == null)
                return NotFound();

            string filePath = await this._filesStorageService.DownloadFile(id);

            // Anonymous type to returns a Json

            var result = new { path = filePath };

            return Helper.SerializeObject(result);
        }

        /// <summary>
        /// Update a specific activity
        /// </summary>
        /// <param name="file">File to uload</param>
        /// <returns>No content</returns>
        [HttpPut]
        public async Task<ActionResult<ActivityDto>> UpdateActivityAsync([FromForm] IFormFile file)
        {
            if (file == null || file.Length < 1)
            {
                return BadRequest("File is empty.");
            }

            // Extract object from incoming JSON file 

            ActivityDto activity = Helper.DeserializeFromStream<ActivityDto>(file.OpenReadStream());
            Activity updatedActivity = new Activity(activity);

            // Update in our database

            await this._activitiesRepository.UpdateActivityAsync(updatedActivity);

            // Upload the file to our online storage

            if (!(await this._filesStorageService.UploadAsync(updatedActivity.Id, file)))
            {
                return StatusCode(500);
            }

            return CreatedAtAction(nameof(GetActivityByIdAsync), new { id = updatedActivity.Id }, updatedActivity.AsDto());
        }

        #endregion
    }
}