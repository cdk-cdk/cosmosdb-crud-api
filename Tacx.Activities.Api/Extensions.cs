﻿using Tacx.Activities.Api.Dtos;
using Tacx.Activities.Api.Models;

namespace Tacx.Activities.Api
{
    public static class Extensions
    {
        /// <summary>
        /// Extension allowing us to convert an activity to the domain
        /// </summary>
        /// <param name="activity">Activity we want to transform</param>
        /// <returns>ActivityDto</returns>
        public static ActivityDto AsDto(this Activity activity)
        {
            return new ActivityDto
            {
                ActivityId = activity.Id,
                Name = activity.Name,
                Description = activity.Description,
                Distance = activity.Distance,
                Duration = activity.Duration,
                AvgSpeed = activity.AvgSpeed,
                CreatedDate = activity.CreatedDate
            };
        }
    }
}
