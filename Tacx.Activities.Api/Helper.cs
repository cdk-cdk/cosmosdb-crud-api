﻿using System.IO;
using System.Text.Json;

namespace Tacx.Activities.Api
{
    /// <summary>
    /// Class providing helpful methods
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Deserialize any object
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="stream">Stream from which we want to extract the object</param>
        /// <returns>An instance of T</returns>
        public static T DeserializeFromStream<T>(Stream stream)
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };

            using StreamReader reader = new StreamReader(stream);
            return JsonSerializer.Deserialize<T>(reader.ReadToEnd(), options);
        }

        /// <summary>
        /// Serialize any object
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="obj">Object we want to serialize</param>
        /// <returns>String containing our object in Json format</returns>
        public static string SerializeObject<T>(T obj)
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };

            return JsonSerializer.Serialize(obj, options);
        }
    }
}
