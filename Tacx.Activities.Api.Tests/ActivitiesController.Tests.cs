using Moq;
using System;
using Xunit;
using FluentAssertions;
using Tacx.Activities.Api.Repository;
using Tacx.Activities.Api.Services;
using Tacx.Activities.Api.Controllers;
using Tacx.Activities.Api.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tacx.Activities.Api.Models;
using Microsoft.AspNetCore.Http;

namespace Tacx.Activities.Api.Tests
{
    /// <summary>
    /// Test class for ActivitiesController
    /// </summary>
    public class ActivitiesControllerTests
    {
        #region Instance variables

        private readonly Mock<IActivitiesRepository> _repositoryStub = new Mock<IActivitiesRepository>();
        private readonly Mock<IFilesStorageService> _serviceStub = new Mock<IFilesStorageService>();
        private readonly Random _rand = new Random();

        #endregion

        #region Public methods

        /// <summary>
        /// Test for CreateActivityAsync
        /// We ensures that a new activity will be created
        /// </summary>
        [Fact]
        public async Task CreateActivityAsync_WithItemToCreate_ReturnsCreatedItem()
        {
            // Arrange

            ActivityDto activityToCreate = TestsHelper.CreateRandomActivityDto();
            FormFile formFile = TestsHelper.CreateMockFormFile(Helper.SerializeObject(activityToCreate));

            this._repositoryStub.Setup(repo => repo.AddActivityAsync(It.IsAny<Activity>()))
                .ReturnsAsync(true);
            this._serviceStub.Setup(s => s.UploadAsync(It.IsAny<string>(), It.IsAny<IFormFile>()))
                .ReturnsAsync(true);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.CreateActivityAsync(formFile);

            // Assert

            var createdItem = (result.Result as CreatedAtActionResult).Value as ActivityDto;
            activityToCreate.Should().BeEquivalentTo(
                createdItem,
                options => options.ComparingByMembers<ActivityDto>().ExcludingMissingMembers()
            );

            createdItem.ActivityId.Should().NotBeEmpty();
        }

        /// <summary>
        /// Test for CreateActivityAsync
        /// We ensures that a conflict error is sent
        /// </summary>
        [Fact]
        public async Task CreateActivityAsync_WithExistingActivity_ReturnsConflict()
        {
            // Arrange

            ActivityDto activityToCreate = TestsHelper.CreateRandomActivityDto();
            string fileContent = Helper.SerializeObject(activityToCreate);
            FormFile file = TestsHelper.CreateMockFormFile(fileContent);

            this._repositoryStub.Setup(repo => repo.AddActivityAsync(It.IsAny<Activity>()))
                .ReturnsAsync(false);
            this._serviceStub.Setup(s => s.UploadAsync(It.IsAny<string>(), It.IsAny<IFormFile>()))
                .ReturnsAsync(true);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.CreateActivityAsync(file);

            // Assert

            result.Result.Should().BeOfType<ConflictResult>();
        }

        /// <summary>
        /// Test for DeleteActivityById
        /// Ensures that NoContent is sent when deleting an activity
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task DeleteActivityByIdAsync_WithExistingActivity_ReturnsNoContent()
        {
            // Arrange
            Activity existingActivity = TestsHelper.CreateRandomActivity();

            this._repositoryStub.Setup(repo => repo.GetActivityByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(existingActivity);
            this._serviceStub.Setup(s => s.DeleteFile(It.IsAny<string>()))
                 .ReturnsAsync(true);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.DeleteActivityByIdAsync(existingActivity.Id);

            // Assert

            result.Should().BeOfType<NoContentResult>();
        }

        /// <summary>
        /// Test for DeleteActivityById
        /// Ensures that NoContent is sent when deleting an activity
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task DeleteActivityByIdAsync_WithNoActivity_ReturnsNotFound()
        {
            // Arrange
            Activity existingActivity = null;

            this._repositoryStub.Setup(repo => repo.GetActivityByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(existingActivity);
            this._serviceStub.Setup(s => s.DeleteFile(It.IsAny<string>()))
                 .ReturnsAsync(false);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.DeleteActivityByIdAsync("ANYID");

            // Assert

            result.Should().BeOfType<NotFoundResult>();
        }

        /// <summary>
        /// Test for DeleteActivityById
        /// Ensures that NoContent is sent when deleting an activity
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task DeleteActivityByIdAsync_WithNoId_ReturnsNotFound()
        {
            // Arrange

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.DeleteActivityByIdAsync("");

            // Assert

            result.Should().BeOfType<BadRequestResult>();
        }

        /// <summary>
        /// Test for GetActivityByIdAsync
        /// Ensures that it returns Bad Request when no id is provided
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task GetActivityByIdAsync_WithEmptyId_ReturnsBadRequest()
        {
            // Arrange

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.GetActivityByIdAsync(String.Empty);

            // Assert

            result.Result.Should().BeOfType<BadRequestResult>();
        }

        /// <summary>
        /// Test for GetActivityByIdAsync
        /// Ensures that it returns the activity when it exists
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task GetActivityByIdAsync_WithMatchingActivity_ReturnsMatchingItem()
        {
            // Arrange

            Activity expectedActivity = TestsHelper.CreateRandomActivity();

            this._repositoryStub.Setup(repo => repo.GetActivityByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(expectedActivity);
            this._serviceStub.Setup(s => s.DownloadFile(It.IsAny<string>()))
                .ReturnsAsync("destinationPath");

            string expected = Helper.SerializeObject(new { path = "destinationPath" });

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.GetActivityByIdAsync(expectedActivity.Id);

            // Assert

            result.Value.Should().BeEquivalentTo(expected);
        }

        /// <summary>
        /// Test for GetActivityByIdAsync
        /// Ensures that it returns not found when the activity doesn't exist
        /// </summary>
        /// <returns>Task</returns>
        [Fact]
        public async Task GetActivityByIdAsync_WithUnexistingActivity_ReturnsNotFound()
        {
            // Arrange

            this._repositoryStub.Setup(repo => repo.GetActivityByIdAsync(It.IsAny<string>()))
                .ReturnsAsync((Activity)null);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.GetActivityByIdAsync("id");

            // Assert

            result.Result.Should().BeOfType<NotFoundResult>();
        }

        /// <summary>
        /// Test for UpdateActivityAsyn
        /// Ensures that it returns the updated activity
        /// </summary>
        /// <returns>Updated activity</returns>
        [Fact]
        public async Task UpdateActivityAsync_WithExistingActivity_ReturnsUpdatedActivity()
        {
            // Arrange
            Activity existingItem = TestsHelper.CreateRandomActivity();

            var itemToUpdate = existingItem.AsDto();
            itemToUpdate.Distance = this._rand.NextDouble() * 1000;
            itemToUpdate.Duration = this._rand.NextDouble() * 1000;

            FormFile formFile = TestsHelper.CreateMockFormFile(Helper.SerializeObject(itemToUpdate));


            this._repositoryStub.Setup(repo => repo.GetActivityByIdAsync(It.IsAny<string>()))
                 .ReturnsAsync(new Activity(itemToUpdate));
            this._serviceStub.Setup(s => s.UploadAsync(It.IsAny<string>(), It.IsAny<IFormFile>()))
                 .ReturnsAsync(true);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.UpdateActivityAsync(formFile);

            // Assert

            var updatedItem = (result.Result as CreatedAtActionResult).Value as ActivityDto;
            itemToUpdate.Should().BeEquivalentTo(
                updatedItem,
                options => options.ComparingByMembers<ActivityDto>().ExcludingMissingMembers()
            );
        }

        /// <summary>
        /// Test for UpdateActivityAsyn
        /// Ensures that it returns the updated activity even if the activity doesn't exist
        /// </summary>
        /// <returns>Updated activity</returns>
        [Fact]
        public async Task UpdateActivityAsync_WithNoActivity_ReturnsUpdatedActivity()
        {
            // Arrange

            var itemToUpdate = TestsHelper.CreateRandomActivityDto();
            FormFile formFile = TestsHelper.CreateMockFormFile(Helper.SerializeObject(itemToUpdate));


            this._repositoryStub.Setup(repo => repo.GetActivityByIdAsync(It.IsAny<string>()))
                 .ReturnsAsync(new Activity(itemToUpdate));
            this._serviceStub.Setup(s => s.UploadAsync(It.IsAny<string>(), It.IsAny<IFormFile>()))
                 .ReturnsAsync(true);

            var controller = new ActivitiesController(this._repositoryStub.Object, this._serviceStub.Object);

            // Act

            var result = await controller.UpdateActivityAsync(formFile);

            // Assert

            var updatedItem = (result.Result as CreatedAtActionResult).Value as ActivityDto;
            itemToUpdate.Should().BeEquivalentTo(
                updatedItem,
                options => options.ComparingByMembers<ActivityDto>().ExcludingMissingMembers()
            );
        }

        #endregion
    }
}