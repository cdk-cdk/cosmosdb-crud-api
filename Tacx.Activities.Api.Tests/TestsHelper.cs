﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Tacx.Activities.Api.Dtos;
using Tacx.Activities.Api.Models;

namespace Tacx.Activities.Api.Tests
{
    /// <summary>
    /// Class providing helpful methods for testing
    /// </summary>
    public static class TestsHelper
    {
        #region Static variables

        static readonly Random random;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        static TestsHelper()
        {
            random = new Random();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create a random Activity
        /// </summary>
        /// <returns>Activity</returns>
        public static Activity CreateRandomActivity()
        {
            return new Activity()
            {
                Id = Guid.NewGuid().ToString(),
                Name = "Training " + random.Next(1000).ToString(),
                Description = "Description " + random.Next(1000).ToString(),
                Distance = random.NextDouble() * 1000,
                Duration = random.NextDouble() * 1000,
                AvgSpeed = random.NextDouble() * 1000,
                CreatedDate = DateTimeOffset.UtcNow
            };
        }

        /// <summary>
        /// Create a random ActivityDto
        /// </summary>
        /// <returns>ActivityDto</returns>
        public static ActivityDto CreateRandomActivityDto()
        {
            return new ActivityDto()
            {
                ActivityId = Guid.NewGuid().ToString(),
                Name = "Training " + random.Next(1000).ToString(),
                Description = "Description " + random.Next(1000).ToString(),
                Distance = random.NextDouble() * 1000,
                Duration = random.NextDouble() * 1000,
                AvgSpeed = random.NextDouble() * 1000,
                CreatedDate = DateTimeOffset.UtcNow
            };
        }

        /// <summary>
        /// Create a temporary file on disk.
        /// </summary>
        /// <param name="content">Content for the file.</param>
        /// <returns>FormFile ready to use</returns>
        public static FormFile CreateMockFormFile(string content)
        {
            string fileName = Path.GetTempPath() + Guid.NewGuid().ToString() + ".json";
            File.WriteAllText(fileName, content);
            Stream stream = File.OpenRead(fileName);

            FormFile file = new FormFile(stream, 0, stream.Length, "mockName", fileName)
            {
                Headers = new HeaderDictionary(),
                ContentType = "application/json"
            };

            return (file);
        }

        #endregion
    }
}
